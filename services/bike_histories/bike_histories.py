import influxdb
import datetime
import time
import json
import argparse
import shutil
from typing import *

MEASUREMENT_NAME = 'bike_stations'
BIKE_TAG_NAME='bike'
STATION_FIELD_NAME='station'

def get_bike_histories(client: influxdb.InfluxDBClient, n_last_points: int, max_history_days: int) -> \
    Dict[int, Dict[datetime.datetime, int]]:
    bikes = {}
    res = client.query(f'SELECT {STATION_FIELD_NAME} FROM {MEASUREMENT_NAME} \
                         WHERE time >= now() - {max_history_days}d \
                         GROUP BY {BIKE_TAG_NAME} ORDER BY time DESC \
                         LIMIT {n_last_points}',
                         chunk_size=5000, chunked=True)
    for (_, tags), points in res.items():
        bike = tags[BIKE_TAG_NAME]
        station_history = []
        for point in points:
            t = point['time']
            station = point[STATION_FIELD_NAME]
            station_history.append((t, station))
        bikes[bike] = station_history
    return bikes


def next_action_time(period_seconds: int, offset: int, min_wait_seconds: int = 1) -> float:
    """
    Calculates next call time for a task that should be executer periodically.
    E.g. with arguments (60, 5) will return the soonest timestamp 5 seconds after a full minute
    :param period_seconds: Period length
    :param offset: Returned times are every round `period_seconds` + `offset`
    :param min_wait_seconds: minimal time before next call
    :return: int timestamp
    """
    t = time.time()
    res = t // period_seconds * period_seconds + offset
    if res < t + min_wait_seconds:
        res += period_seconds
    return res


def main():
    parser = argparse.ArgumentParser(
        description='Periodically generate a file with bike-station histories',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('dsn',
        help='DSN to the database, including the database name, e.g. '\
        'influxdb://localhost:8086/bikes ')
    parser.add_argument('output_path', help='path to the output file')
    parser.add_argument('-n', '--n_last_points',
                        help='How many last points should be fetched per bike',
                        default=5, type=int)
    parser.add_argument('-d', '--max_history_days', help='Look at most this many days in the past',
                        default=14, type=int)
    parser.add_argument('-i', '--interval', help='wait period between runs [s]',
                        default=60, type=int)
    parser.add_argument('-o', '--offset', help='Returned times are every round `period_seconds` + `offset`',
                        default=30, type=int)

    args = parser.parse_args()
    client = influxdb.InfluxDBClient.from_dsn(args.dsn, timeout=120)
    while True:
        histories = get_bike_histories(client, args.n_last_points, args.max_history_days)
        if len(histories) < 1:
            time.sleep(5)
            print('No histories fetched')
            continue

        temp_filename = args.output_path + '.tmp'
        with open(temp_filename, 'w') as f:
            json.dump(histories, f)
        shutil.move(temp_filename, args.output_path)
        print(f'{datetime.datetime.now()} - wrote histories for '\
              f'{len(histories)} bikes')
        time.sleep(next_action_time(args.interval, args.offset) - time.time())

if __name__ == "__main__":
    main()
