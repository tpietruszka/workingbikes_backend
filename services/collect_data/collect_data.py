import argparse
import requests
import json.decoder
import time
import datetime
import influxdb
from typing import *

MEASUREMENT_NAME = 'bike_stations'
BIKE_TAG_NAME = 'bike'
STATION_FIELD_NAME = 'station'
BIKE_POSITION_UNKNOWN = -1

dt_format_string = '%a, %d %b %Y %H:%M:%S %Z'
max_lookup_days = 28  # limit size of the initial DB lookup, to finish in
# a reasonable time


def timestamp_from_source(response: requests.Response) -> datetime.datetime:
    try:
        timestamp = response.headers['Last-Modified']
    except KeyError:
        print('"Last-Modified" header missing in the response')
        timestamp = response.headers['Date']
    return datetime.datetime.strptime(timestamp, dt_format_string)


def bike_positions_from_source(response: requests.Response) -> Dict[int, int]:
    data = response.json()
    # TODO: alert about missing fields - they should not exist in well-formed data
    places = (place for country in data['countries'] for city in country.get('cities', []) for place in
              city.get('places', []))
    bike_positions = {bike['number']: place['uid'] for place in places for bike in place.get('bike_list', []) if
                      'number' in bike.keys() and 'uid' in place.keys()}
    return bike_positions


def store_positions(client: influxdb.InfluxDBClient, positions: Dict[int, int],
                    timestamp: datetime.datetime) -> bool:
    influx_points = [{'measurement': MEASUREMENT_NAME, 'tags': {BIKE_TAG_NAME: bike_number}, 'time': timestamp,
                      'fields': {STATION_FIELD_NAME: station_number}}
                     for bike_number, station_number in positions.items()]
    return client.write_points(influx_points, time_precision='s', batch_size=5000)


def bike_positions_from_db(client):
    dbr = client.query(f'SELECT last(*) FROM {MEASUREMENT_NAME} \
                         WHERE time >= now() - {max_lookup_days}d \
                         GROUP BY {BIKE_TAG_NAME}', chunk_size=5000)
    bike_stations = {}
    for (_, tags), points in dbr.items():
        bike = tags[BIKE_TAG_NAME]
        for point in points:
            station = point[f'last_{STATION_FIELD_NAME}']
            bike_stations[bike] = station
    return bike_stations


def main():
    parser = argparse.ArgumentParser(
        description='Collect data from nextbike and store them into influxdb')
    parser.add_argument('dsn',
        help='DSN to the database, including the database name, e.g. \
        influxdb://localhost:8086/bikes ')
    args = parser.parse_args()

    client = influxdb.InfluxDBClient.from_dsn(args.dsn, timeout=120)
    positions = bike_positions_from_db(client)
    while True:
        try:
            response = requests.get('https://api.nextbike.net/maps/nextbike-live.json', timeout=10)
            response.raise_for_status()
        except Exception as e:
            print(e)
            time.sleep(5)
            continue
        t = timestamp_from_source(response)
        try:
            current_positions = bike_positions_from_source(response)
        except json.decoder.JSONDecodeError as e:
            print(e)
            time.sleep(5)
            continue
        if len(current_positions) == 0:
            continue

        bikes_not_parked = set(positions.keys()) - set(current_positions.keys())
        for bike in bikes_not_parked:
            current_positions[bike] = BIKE_POSITION_UNKNOWN
        position_updates = {bike: position for bike, position in current_positions.items() if
                            bike not in positions.keys() or positions[bike] != position}
        print(f'Position updates: {len(position_updates)} at {t}')
        _ = store_positions(client, positions=position_updates, timestamp=t)
        positions = current_positions
        time.sleep(60)


if __name__ == "__main__":
    main()
